package Vdb;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerSocketListener extends Thread {
	private ServerSocket server_socket_to_slaves = null;

	public ServerSocketListener(ServerSocket server_socket_to_slaves) {
		this.server_socket_to_slaves = server_socket_to_slaves;
	}

	@Override
	public void run() {
		common.ptod("--------服务端监听端口开始--------");
		try {
			while(!server_socket_to_slaves.isClosed()) {
				SlaveSocket socket = new SlaveSocket(server_socket_to_slaves);
	
				common.ptod("服务端监听到一个客户端连接，等待确认通讯是否正常...");
	
				/* We have a new client, hand him off to a separate thread: */
				SlaveOnMaster som = new SlaveOnMaster();
				som.setSocket(socket);
				som.start();
			}
		}
		catch (IOException e) {
		}
		common.ptod("--------服务端监听端口结束--------");
	}
}
